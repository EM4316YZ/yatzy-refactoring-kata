package com.kata.yatzy.category.enums;

public enum NumberCategoryEnum {
    ONES(1),
    TWOS(2),
    THREES(3),
    FOURS(4),
    FIVES(5),
    SIXES(6);

    private final int number;

    NumberCategoryEnum(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
