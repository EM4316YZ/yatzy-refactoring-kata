package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;
import com.kata.yatzy.category.enums.NumberCategoryEnum;

import java.util.List;

public class NumberCategory implements YatzyCategoryDecorator {
    NumberCategoryEnum numberCategoryEnum;

    public NumberCategory(NumberCategoryEnum numberCategoryEnum) {
        this.numberCategoryEnum = numberCategoryEnum;
    }

    @Override
    public int calculateScore(List<Integer> diceResults) {
        return diceResults.stream().filter(item -> item == numberCategoryEnum.getNumber())
                .mapToInt(Integer::intValue).sum();
    }
}
