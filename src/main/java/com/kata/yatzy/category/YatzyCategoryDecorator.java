package com.kata.yatzy.category;

import java.util.List;

public interface YatzyCategoryDecorator {
    int calculateScore(List<Integer> diceResults);
}
