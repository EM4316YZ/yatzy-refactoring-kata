package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;
import com.kata.yatzy.category.utils.YatzyUtils;

import java.util.List;

public class FullHouseCategory implements YatzyCategoryDecorator {
    @Override
    public int calculateScore(List<Integer> diceResults) {
        List<Integer> listWith3Repetition = YatzyUtils.calculateRepeatDiceResults(diceResults, 3);
        if (listWith3Repetition.isEmpty()) {
            return 0;
        }
        List<Integer> listWith2Repetition = YatzyUtils.calculateRepeatDiceResults(diceResults.stream()
                .filter(item -> !item.equals(listWith3Repetition.getLast()))
                .toList(), 2);
        return listWith2Repetition.isEmpty() ? 0 : listWith3Repetition.getLast() * 3 + listWith2Repetition.getLast() * 2;
    }
}
