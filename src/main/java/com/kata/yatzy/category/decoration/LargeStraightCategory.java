package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;

import java.util.List;

public class LargeStraightCategory implements YatzyCategoryDecorator {
    @Override
    public int calculateScore(List<Integer> diceResults) {
        return diceResults.equals(List.of(2, 3, 4, 5, 6)) ? 20 : 0;
    }
}
