package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;

import java.util.List;

public class YatzyCategory implements YatzyCategoryDecorator {
    @Override
    public int calculateScore(List<Integer> diceResults) {
        return diceResults.stream().distinct().count() == 1 ? 50 : 0;
    }
}
