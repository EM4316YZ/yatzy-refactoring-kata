package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;

import java.util.List;

public class ChanceCategory implements YatzyCategoryDecorator {
    @Override
    public int calculateScore(List<Integer> diceResults) {
        return diceResults.stream().mapToInt(Integer::intValue).sum();
    }
}
