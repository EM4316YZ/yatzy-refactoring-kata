package com.kata.yatzy.category.utils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class YatzyUtils {

    public static List<Integer> calculateRepeatDiceResults(List<Integer> diceResults, int repeatingNumber) {
        return diceResults.stream()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .entrySet().stream().filter(item -> item.getValue() >= repeatingNumber)
                .map(Map.Entry::getKey).toList();
    }
}
