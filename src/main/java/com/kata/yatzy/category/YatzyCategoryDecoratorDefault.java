package com.kata.yatzy.category;

import java.util.List;
import java.util.Objects;

public class YatzyCategoryDecoratorDefault implements YatzyCategoryDecorator {

    private final YatzyCategoryDecorator wrapper;

    public YatzyCategoryDecoratorDefault(YatzyCategoryDecorator yatzyCategoryDecorator) {
        this.wrapper = yatzyCategoryDecorator;
    }

    @Override
    public int calculateScore(List<Integer> diceResults) {
        validationOfInputData(diceResults);
        return wrapper.calculateScore(diceResults);
    }

    private void validationOfInputData(List<Integer> diceResults) {
        if (diceResults == null || diceResults.isEmpty() || diceResults.stream().anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("Dice result should be not null or empty or contains null");
        }
        if (diceResults.size() != 5) {
            throw new IllegalArgumentException("Dice result should equal to 5");
        }
    }
}
