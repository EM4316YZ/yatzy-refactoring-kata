package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;

import java.util.List;

public class SmallStraightCategory implements YatzyCategoryDecorator {
    @Override
    public int calculateScore(List<Integer> diceResults) {
        return diceResults.equals(List.of(1, 2, 3, 4, 5)) ? 15 : 0;
    }
}
