# yatzy-refactoring-kata
## Description
The game of Yatzy is a simple dice game. Each player rolls five six-sided dice. They can re-roll some or all of the dice up to three times (including the original roll).

The player then places the roll in a category, such as ones, twos, fives, pair, two pairs etc (see the rules below). If the roll is compatible with the category, the player gets a score for the roll according to the rules. If the roll is not compatible with the category, the player scores zero for the roll.

More information [here](https://sammancoaching.org/kata_descriptions/yatzy.html)

## Language and version
| Language     | Version |
|--------------|---------|
| JAVA         | 21      |
| JUnit        | 5.10.0  |
| JUnit params | 5.10.0  |  
| Maven        | 3.9.2   |  

## Algo
We will use on this project the design pattern DECORATOR to calculate score according to category

We have 15 categories

We used TDD to implement the codes

| Category       | Class                       | Comment                                                                                         |
|----------------|-----------------------------|-------------------------------------------------------------------------------------------------|
| Chance         | new ChanceCategory()        |                                                                                                 |
| Four Kind      | new FourKindCategory()      |                                                                                                 |
| Full House     | new FullHouseCategory()     |                                                                                                 |
| Large Straight | new LargeStraightCategory() |                                                                                                 |
| Number         | new NumberCategory(enum)    | use NumberCategoryEnum to choose your category number (ONES, TWOS, THREES, FOURS, FIVES, SIXES) |
| Pair           | new PairCategoryDefault()   |                                                                                                 |
| Small Straight | new SmallStraightCategory() |                                                                                                 |
| Three Kind     | new ThreeKindCategory()     |                                                                                                 |
| Two Pair       | new TwoPairCategory()       |                                                                                                 |
| Yatzy          | new YatzyCategory()         |                                                                                                 |


## Example
#### Chance Category
```
void main() {
    YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new ChanceCategory());
    System.out.println(yatzyCategoryDecorator.calculateScore(diceResults));
}
```
#### Number Category 
```
void main() {
    YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new NumberCategory(NumberCategoryEnum.Fours));
    System.out.println(yatzyCategoryDecorator.calculateScore(diceResults));
}
```

```diceResults``` should be not null, not empty, does not contain null and should contain 5 element