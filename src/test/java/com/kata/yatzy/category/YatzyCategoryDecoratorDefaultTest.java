package com.kata.yatzy.category;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.kata.yatzy.category.decoration.*;
import com.kata.yatzy.category.enums.NumberCategoryEnum;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

class YatzyCategoryDecoratorDefaultTest{

    @ParameterizedTest
    @MethodSource("generateDataForException")
    void should_get_illegal_arguments_when_list_not_respect_condition(String exceptedExceptionMessage, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new ChanceCategory());
        // When & Then
        assertThrows(IllegalArgumentException.class, () -> yatzyCategoryDecorator.calculateScore(diceResults),
                exceptedExceptionMessage);
    }

    @ParameterizedTest
    @MethodSource("generateDataForChanceCategory")
    void should_get_score_when_choose_chance_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new ChanceCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForYatzyCategory")
    void should_get_score_when_choose_yatzy_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new YatzyCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForNumberCategory")
    void should_get_score_when_choose_number_category(int expected, NumberCategoryEnum numberCategoryEnum, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new NumberCategory(numberCategoryEnum));
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForPairCategory")
    void should_get_score_when_choose_pair_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new PairCategoryDefault());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForTwoPairCategory")
    void should_get_score_when_choose_two_pair_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new TwoPairCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForThreeKindCategory")
    void should_get_score_when_choose_three_kind_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new ThreeKindCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForFourKindCategory")
    void should_get_score_when_choose_four_kind_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new FourKindCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForSmallStraightCategory")
    void should_get_score_when_choose_small_straight_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new SmallStraightCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForLargeStraightCategory")
    void should_get_score_when_choose_large_straight_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new LargeStraightCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }

    @ParameterizedTest
    @MethodSource("generateDataForFullHouseCategory")
    void should_get_score_when_choose_full_house_category(int expected, List<Integer> diceResults) {
        // Given
        YatzyCategoryDecorator yatzyCategoryDecorator = new YatzyCategoryDecoratorDefault(new FullHouseCategory());
        // When
        int score = yatzyCategoryDecorator.calculateScore(diceResults);
        // Then
        assertEquals(expected, score);
    }
    static Stream<Arguments> generateDataForException() {
        return Stream.of(
                Arguments.of("Dice result should be not null or empty or contains null", null),
                Arguments.of("Dice result should be not null or empty or contains null", Collections.emptyList()),
                Arguments.of("Dice result should be not null or empty or contains null", Arrays.asList(1, 1, 3, null, 6, 5)),
                Arguments.of("Dice result should equal to 5", List.of(1, 1, 3, 3, 6, 5)),
                Arguments.of("Dice result should equal to 5", List.of(4, 5, 5, 6)));
    }

    static Stream<Arguments> generateDataForChanceCategory() {
        return Stream.of(
                Arguments.of(14, List.of(1, 1, 3, 3, 6)),
                Arguments.of(21, List.of(4, 5, 5, 6, 1)));
    }

    static Stream<Arguments> generateDataForYatzyCategory() {
        return Stream.of(
                Arguments.of(50, List.of(1, 1, 1, 1, 1)),
                Arguments.of(50, List.of(2, 2, 2, 2, 2)),
                Arguments.of(50, List.of(6, 6, 6, 6, 6)),
                Arguments.of(0, List.of(1, 1, 1, 2, 1)));
    }

    static Stream<Arguments> generateDataForNumberCategory() {
        return Stream.of(
                Arguments.of(8, NumberCategoryEnum.FOURS, List.of(1, 1, 2, 4, 4)),
                Arguments.of(0, NumberCategoryEnum.ONES, List.of(3, 3, 3, 4, 5)),
                Arguments.of(4, NumberCategoryEnum.TWOS, List.of(2, 3, 2, 5, 1)));
    }

    static Stream<Arguments> generateDataForPairCategory() {
        return Stream.of(
                Arguments.of(0, List.of(1, 2, 3, 4, 5)),
                Arguments.of(8, List.of(3, 3, 3, 4, 4)),
                Arguments.of(12, List.of(1, 1, 6, 2, 6)),
                Arguments.of(12, List.of(6, 6, 6, 2, 6)),
                Arguments.of(6, List.of(3, 3, 3, 4, 1)),
                Arguments.of(6, List.of(3, 3, 3, 3, 1)));
    }

    static Stream<Arguments> generateDataForTwoPairCategory() {
        return Stream.of(
                Arguments.of(8, List.of(1, 1, 2, 3, 3)),
                Arguments.of(0, List.of(1, 1, 2, 3, 4)),
                Arguments.of(6, List.of(1, 1, 2, 2, 2)),
                Arguments.of(0, List.of(3, 3, 3, 3, 1))
        );
    }

    static Stream<Arguments> generateDataForThreeKindCategory() {
        return Stream.of(
                Arguments.of(9, List.of(3, 3, 3, 4, 5)),
                Arguments.of(0, List.of(3, 3, 4, 5, 6)),
                Arguments.of(9, List.of(3, 3, 3, 3, 1)),
                Arguments.of(9, List.of(3, 3, 4, 5, 3)),
                Arguments.of(18, List.of(6, 6, 4, 5, 6)));
    }

    static Stream<Arguments> generateDataForFourKindCategory() {
        return Stream.of(
                Arguments.of(8, List.of(2, 2, 2, 2, 5)),
                Arguments.of(0, List.of(2, 2, 2, 5, 5)),
                Arguments.of(24, List.of(6, 6, 2, 6, 6)),
                Arguments.of(8, List.of(2, 2, 2, 2, 2)));
    }

    static Stream<Arguments> generateDataForSmallStraightCategory() {
        return Stream.of(
                Arguments.of(15, List.of(1,2,3,4,5)),
                Arguments.of(0, List.of(1,3,2,4,5)),
                Arguments.of(0, List.of(1,3,2,4,6)),
                Arguments.of(0, List.of(2,3,4,5,6)),
                Arguments.of(0, List.of(2,3,2,4,5)));
    }

    static Stream<Arguments> generateDataForLargeStraightCategory() {
        return Stream.of(
                Arguments.of(0, List.of(1,2,3,4,5)),
                Arguments.of(0, List.of(1,3,2,4,5)),
                Arguments.of(0, List.of(1,3,2,4,6)),
                Arguments.of(20, List.of(2,3,4,5,6)),
                Arguments.of(0, List.of(2,3,2,4,5)));
    }


    static Stream<Arguments> generateDataForFullHouseCategory() {
        return Stream.of(
                Arguments.of(8, List.of(1,1,2,2,2)),
                Arguments.of(22, List.of(6,6,6,2,2)),
                Arguments.of(0, List.of(2,2,3,3,4)),
                Arguments.of(0, List.of(4,4,4,4,4))
        );
    }
}
