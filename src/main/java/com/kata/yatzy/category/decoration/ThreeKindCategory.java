package com.kata.yatzy.category.decoration;

import com.kata.yatzy.category.YatzyCategoryDecorator;
import com.kata.yatzy.category.utils.YatzyUtils;

import java.util.List;

public class ThreeKindCategory implements YatzyCategoryDecorator {

    @Override
    public int calculateScore(List<Integer> diceResults) {
        List<Integer> list = YatzyUtils.calculateRepeatDiceResults(diceResults, 3);
        return !list.isEmpty() ? list.getLast() * 3 : 0;
    }
}
